---
title: "Converted To Emacs"
date: 2021-07-11T21:36:31-03:00
draft: true
categories:
  - Computing
---

One of the most controversial topics in the Linux community is choice of text editor.  Of course there are a plethora of grahically-based editors such as Atom, Kate, Geary, and others.  However, those who want to really embrace the Unix/Linux philosphy will want to use a command-line based text editor.  The introductory command-line text editor is Nano, but this is almost always a brief stopping point on the way to the two largest competitors for command-line text editors, **Vim** and **Emacs**.  This isn't meant as a comparison between the two, rather, I'd like to establish that I have used Vim now for several years, so I'm quite familar with it - although admittedly I'm not an expert, just a competent user.  Rather, about a month ago, I thought I'd give Emacs a shot.

# Motivations for Using Emacs

Why would I choose to give Emacs, a text-editor known for having a very steep learning curve, a try?  The main motivation was actually /folding/, I wanted to fold functions in a programs so I could see the outline of the program at a glance.  Interestingly, I've not found a great way to fold functions - although ```hs-minor-mode``` does work.  The default keybindings are not easy to remmeber - but they could be changed or a different package could be found or even written.

# A Quick History of Emacs

Emacs was written by Richard Stallman - one of the most important figures in Free Software (and computing in general).  It was his 