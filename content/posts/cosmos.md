---
title: "Cosmos"
date: 2021-01-30
categories:
  - Reading
draft: false
---

{{< image src="/readingJournal/cosmos.jpg" alt="Cosmos" position="center" style="border-radius: 8px;" >}}

Written by Carl Sagan in 1980 to accompany the PBS series of the same name, Cosmos is a science classic that is well worth the read.  Although severely dated in the past 40 years as scientific discovery has marched on, this is one of the best popular science books ever written.  Completely up to date in its time, it outlines our position in the universe, the possibility of life on other planets, the miracle of life on this planet, and how a very small change billions of years ago could have sent life on this planet in a very different trajectory.
Cosmos by Carl Sagan

If this book were updated with 2021’s scientific knowledge, it would be an absolutely fascinating read.  It still shines like a diamond, although as though we’re looking through the cloudy glass at a brilliant diamond.  Of course, this is always the challenge with popular science writings, as new discoveries are made, older writings age.  However, the clarity and passion of Mr. Sagan’s writing make it an absolute masterpiece, well worth the investment of time.


